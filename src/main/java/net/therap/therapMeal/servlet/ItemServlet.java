package net.therap.therapMeal.servlet;

import net.therap.therapMeal.domain.Item;
import net.therap.therapMeal.service.ItemService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(value = "/items")
public class ItemServlet extends HttpServlet {

    public void service(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String action = request.getParameter("itemAction");

        if (action.equals("show")) {
            showItems(request, response);

        } else if (action.equals("add")) {
            insertItem(request, response);

        } else if (action.equals("addNewItem")) {
            addNewItemCall(request, response);

        } else {
            action = action.split(":")[0];

            if (action.equals("update")) {
                updateItem(request, response);

            } else if (action.equals("remove")) {
                removeItem(request, response);

            } else {
                updateItemCall(request, response);
            }
        }
    }

    public void showItems(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ItemService itemService = new ItemService();
        List<Item> items = itemService.getItems();

        request.setAttribute("items", items);

        RequestDispatcher rd = request.getRequestDispatcher("item.jsp");
        rd.forward(request, response);
    }

    public void insertItem(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String itemName = request.getParameter("itemName");

        ItemService itemService = new ItemService();
        itemService.addNewItem(itemName);

        showItems(request, response);
    }

    public void removeItem(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("itemAction");
        int itemId = Integer.parseInt(action.split(":")[1]);

        Item item = new Item();
        item.setId(itemId);

        ItemService itemService = new ItemService();
        itemService.removeItem(item);

        showItems(request, response);
    }

    public void updateItem(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("itemAction");
        int itemId = Integer.parseInt(action.split(":")[1]);
        String itemName = request.getParameter("itemName");

        ItemService itemService = new ItemService();
        itemService.updateItem(itemId, itemName);

        showItems(request, response);
    }

    public void updateItemCall(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("itemAction");
        int itemId = Integer.parseInt(action.split(":")[1]);

        request.setAttribute("itemId", itemId);
        request.setAttribute("action", "update");

        RequestDispatcher rd = request.getRequestDispatcher("updateOrAddItem.jsp");
        rd.forward(request, response);
    }

    public void addNewItemCall(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("action", "add");
        RequestDispatcher rd = request.getRequestDispatcher("updateOrAddItem.jsp");
        rd.forward(request, response);
    }
}
