package net.therap.therapMeal.servlet;

import net.therap.therapMeal.domain.MealType;
import net.therap.therapMeal.service.MealTypeService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(value = "/mealTypes")
public class MealTypeServlet extends HttpServlet {

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("mealTypeAction");

        if (action.equals("show")) {
            showMealTypes(request, response);

        } else if (action.equals("add")) {
            addMealType(request, response);

        } else if (action.equals("addNewMealType")) {
            addNewMealTypeCall(request, response);

        } else {
            action = action.split(":")[0];

            if (action.equals("update")) {
                updateMealType(request, response);

            } else if (action.equals("remove")) {
                removeMealType(request, response);

            } else {
                updateMealTypeCall(request, response);
            }
        }
    }

    public void showMealTypes(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        MealTypeService mealTypeService = new MealTypeService();
        List<MealType> mealTypes = mealTypeService.getMealTypes();

        request.setAttribute("mealTypes", mealTypes);

        RequestDispatcher rd = request.getRequestDispatcher("mealType.jsp");
        rd.forward(request, response);
    }

    public void addMealType(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String mealTypeName = request.getParameter("mealTypeName");

        MealTypeService mealTypeService = new MealTypeService();
        mealTypeService.addMealType(mealTypeName);

        showMealTypes(request, response);
    }

    public void removeMealType(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("mealTypeAction");
        int mealTypeId = Integer.parseInt(action.split(":")[1]);

        MealType mealType = new MealType();
        mealType.setId(mealTypeId);

        MealTypeService mealTypeService = new MealTypeService();
        mealTypeService.removeMealType(mealTypeId);

        showMealTypes(request, response);
    }

    public void updateMealType(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("mealTypeAction");
        System.out.println(action);
        int mealTypeId = Integer.parseInt(action.split(":")[1]);
        String mealTypeName = request.getParameter("mealTypeName");
        System.out.println(mealTypeId+" "+mealTypeName);

        MealTypeService mealTypeService = new MealTypeService();
        mealTypeService.updateMealType(mealTypeId, mealTypeName);

        showMealTypes(request, response);
    }

    public void addNewMealTypeCall(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("action", "add");
        RequestDispatcher rd = request.getRequestDispatcher("updateOrAddMealType.jsp");
        rd.forward(request, response);
    }

    public void updateMealTypeCall(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("mealTypeAction");
        int mealTypeId = Integer.parseInt(action.split(":")[1]);

        request.setAttribute("mealTypeId", mealTypeId);
        request.setAttribute("action", "update");

        RequestDispatcher rd = request.getRequestDispatcher("updateOrAddMealType.jsp");
        rd.forward(request, response);
    }
}
