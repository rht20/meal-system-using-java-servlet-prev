package net.therap.therapMeal.app;

import net.therap.therapMeal.helper.EntityManagerHelper;

/**
 * @author rakibul.hasan
 * @since 2/11/20
 */
public class Main {

    public static void main(String[] args) {
        EntityManagerHelper entityManagerHelper = new EntityManagerHelper();
        entityManagerHelper.getEntityManager().close();
        entityManagerHelper.close();
    }
}
