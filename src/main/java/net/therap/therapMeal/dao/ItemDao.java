package net.therap.therapMeal.dao;

import net.therap.therapMeal.domain.Item;
import net.therap.therapMeal.helper.EntityManagerHelper;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * @author rakibul.hasan
 * @since 2/16/20
 */
public class ItemDao {

    private EntityManager em;

    public ItemDao() {
        this.em = EntityManagerHelper.getEntityManager();
    }

    public List<Item> getItems() {
        List<Item> itemList = em.createQuery("FROM Item").getResultList();
        return itemList;
    }

    public void insertItem(Item item) {
        em.getTransaction().begin();
        em.persist(item);
        em.getTransaction().commit();
    }

    public void removeItem(Item item) {
        em.getTransaction().begin();
        em.remove(em.contains(item) ? item : em.merge(item));
        em.getTransaction().commit();
    }

    public void updateItem(Item oldItem, Item newItem) {
        TypedQuery<Item> query = em.createQuery("SELECT I FROM Item AS I WHERE I.id = :itemId", Item.class)
                .setParameter("itemId",  oldItem.getId());

        Item item = query.getResultList().get(0);

        em.getTransaction().begin();
        item.setName(newItem.getName());
        em.getTransaction().commit();
    }
}
