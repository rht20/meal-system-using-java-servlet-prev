package net.therap.therapMeal.dao;

import net.therap.therapMeal.domain.MealType;
import net.therap.therapMeal.helper.EntityManagerHelper;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * @author rakibul.hasan
 * @since 2/16/20
 */
public class MealTypeDao {

    private EntityManager em;

    public MealTypeDao() {
        this.em = EntityManagerHelper.getEntityManager();
    }

    public List<MealType> getMealTypes() {
        List<MealType> mealTypeList = em.createQuery("FROM MealType").getResultList();
        return mealTypeList;
    }

    public void insertMealType(MealType mealType) {
        em.getTransaction().begin();
        em.persist(mealType);
        em.getTransaction().commit();
    }

    public void removeMealType(MealType mealType) {
        em.getTransaction().begin();
        em.remove(em.contains(mealType) ? mealType : em.merge(mealType));
        em.getTransaction().commit();
    }

    public void updateMealType(MealType oldMealType, MealType newMealType) {
        TypedQuery<MealType> query = em.createQuery("SELECT M FROM MealType AS M WHERE M.id = :mealTypeId", MealType.class)
                .setParameter("mealTypeId",  oldMealType.getId());

        MealType mealType = query.getResultList().get(0);

        em.getTransaction().begin();
        mealType.setName(newMealType.getName());
        em.getTransaction().commit();
    }
}
