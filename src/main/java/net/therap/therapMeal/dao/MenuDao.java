package net.therap.therapMeal.dao;

import net.therap.therapMeal.domain.*;
import net.therap.therapMeal.helper.EntityManagerHelper;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * @author rakibul.hasan
 * @since 2/16/20
 */
public class MenuDao {

    private EntityManager em;

    public MenuDao() {
        this.em = EntityManagerHelper.getEntityManager();
    }

    public List<Menu> getMenus() {
        List<Menu> menus = em.createQuery("FROM Menu").getResultList();
        return menus;
    }

    public Menu getMenu(MealType mealType, Day day) {
        TypedQuery<Menu> query = em.createQuery("SELECT M FROM Menu AS M WHERE M.mealType = :mealType AND M.day = :day", Menu.class)
                .setParameter("mealType", mealType)
                .setParameter("day", day);

        List<Menu> menuList = query.getResultList();
        Menu menu = menuList.isEmpty() ? new Menu(mealType, day) : menuList.get(0);

        return menu;
    }

    public void insertMenu(MealType mealType, Day day, Item item) {
        TypedQuery<Menu> query = em.createQuery("SELECT M FROM Menu AS M WHERE M.mealType = :mealType AND M.day = :day", Menu.class)
                .setParameter("mealType", mealType)
                .setParameter("day", day);

        List<Menu> menuList = query.getResultList();
        Menu menu = menuList.isEmpty() ? new Menu(mealType, day) : menuList.get(0);
        menu.getItemList().add(item);

        em.getTransaction().begin();
        em.persist(menu);
        em.getTransaction().commit();
    }

    public void removeMenu(Menu menu) {
        TypedQuery<Menu> query = em.createQuery("SELECT M FROM Menu AS M where M.id = :menuId", Menu.class)
                .setParameter("menuId", menu.getId());

        menu = query.getResultList().get(0);

        em.getTransaction().begin();
        em.remove(menu);
        em.getTransaction().commit();
    }

    public void removeByItem(Item item) {
        TypedQuery<Menu> query = em.createQuery("SELECT M FROM Menu AS M", Menu.class);
        List<Menu> menuList = query.getResultList();

        em.getTransaction().begin();
        boolean flag = false;
        for (Menu menu : menuList) {
            for (Item item1 : menu.getItemList()) {
                if (item.getId() == item1.getId()) {
                    menu.getItemList().remove(item1);
                    flag = true;
                    break;
                }
            }
            if (flag) {
                break;
            }
        }
        em.getTransaction().commit();
    }

    public void removeByMealType(MealType mealType) {
        TypedQuery<Menu> query = em.createQuery("SELECT M FROM Menu AS M WHERE M.mealType = :mealType", Menu.class)
                .setParameter("mealType", mealType);
        List<Menu> menuList = query.getResultList();

        em.getTransaction().begin();
        for (Menu menu : menuList) {
            em.remove(menu);
        }
        em.getTransaction().commit();
    }

    public void removeMenuItem(MealType mealType, Day day, Item item) {
        TypedQuery<Menu> query = em.createQuery("SELECT M FROM Menu AS M WHERE M.mealType = :mealType AND M.day = :day", Menu.class)
                .setParameter("mealType", mealType)
                .setParameter("day", day);

        Menu menu = query.getSingleResult();

        em.getTransaction().begin();
        menu.getItemList().remove(em.contains(item) ? item : em.merge(item));
        em.persist(menu);
        em.getTransaction().commit();
    }
}
