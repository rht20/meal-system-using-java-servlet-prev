package net.therap.therapMeal.service;

import net.therap.therapMeal.dao.MenuDao;
import net.therap.therapMeal.domain.Day;
import net.therap.therapMeal.domain.Item;
import net.therap.therapMeal.domain.MealType;
import net.therap.therapMeal.domain.Menu;

import java.util.List;

/**
 * @author rakibul.hasan
 * @since 2/13/20
 */
public class MenuService {

    private MenuDao menuDao;
    private ItemService itemService;
    private MealTypeService mealTypeService;

    public MenuService() {
        this.menuDao = new MenuDao();
        this.itemService = new ItemService();
        this.mealTypeService = new MealTypeService();
    }

    public List<Menu> getMenus() {
        return menuDao.getMenus();
    }

    public Menu getMenu(int mealTypeId, String day) {
        MealType mealType = getMealType(mealTypeId);
        Day day1 = getDay(day);

        Menu menu = menuDao.getMenu(mealType, day1);
        return menu;
    }

    public void addItemToMenu(int mealTypeId, String day, int itemId) {
        MealType mealType = getMealType(mealTypeId);
        Day day1 = getDay(day);
        Item item = getItem(itemId);

        menuDao.insertMenu(mealType, day1, item);
    }

    public void removeMenu(int menuId) {
        Menu menu = new Menu();
        menu.setId(menuId);

        menuDao.removeMenu(menu);
    }

    public void removeItemFromMenu(int mealTypeId, String day, int itemId) {
        MealType mealType = getMealType(mealTypeId);
        Day day1 = getDay(day);
        Item item = getItem(itemId);

        menuDao.removeMenuItem(mealType, day1, item);
    }

    public MealType getMealType(int mealTypeId) {
        List<MealType> mealTypes = mealTypeService.getMealTypes();

        for (MealType mealType : mealTypes) {
            if (mealType.getId() == mealTypeId) {
                return mealType;
            }
        }

        return null;
    }

    public Day getDay(String day) {
        for (Day day1 : Day.values()) {
            if (day1.name().equals(day)) {
                return day1;
            }
        }

        return null;
    }

    public Item getItem(int itemId) {
        List<Item> items = itemService.getItems();

        for (Item item : items) {
            if (item.getId() == itemId) {
                return item;
            }
        }

        return null;
    }
}
