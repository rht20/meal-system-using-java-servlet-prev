package net.therap.therapMeal.service;

import net.therap.therapMeal.domain.Item;
import net.therap.therapMeal.dao.ItemDao;
import net.therap.therapMeal.dao.MenuDao;

import java.util.List;

/**
 * @author rakibul.hasan
 * @since 2/13/20
 */
public class ItemService {

    private ItemDao itemDao;
    private MenuDao menuDao;

    public ItemService() {
        this.itemDao = new ItemDao();
        this.menuDao = new MenuDao();
    }

    public List<Item> getItems() {
        List<Item> itemList = itemDao.getItems();
        return itemList;
    }

    public void addNewItem(String itemName) {
        Item item = new Item();
        item.setName(itemName);
        itemDao.insertItem(item);
    }

    public void removeItem(Item item) {
        menuDao.removeByItem(item);

        List<Item> itemList = getItems();
        for (Item item1 : itemList) {
            if (item.getId() == item1.getId()) {
                itemDao.removeItem(item1);
                break;
            }
        }
    }

    public void updateItem(int itemId, String itemName) {
        Item item1 = new Item();
        item1.setId(itemId);

        Item item2 = new Item();
        item2.setName(itemName);

        itemDao.updateItem(item1, item2);
    }
}
