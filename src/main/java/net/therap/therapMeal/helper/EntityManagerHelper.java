package net.therap.therapMeal.helper;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * @author rakibul.hasan
 * @since 2/23/20
 */
public class EntityManagerHelper {

    public static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("persistence");

//    public EntityManagerHelper() {
//        emf = Persistence.createEntityManagerFactory("persistence");
//    }

    public static EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void close() {
        emf.close();
    }
}
