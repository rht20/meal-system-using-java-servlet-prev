<%--
  Created by IntelliJ IDEA.
  User: rakibul.hasan
  Date: 3/1/20
  Time: 4:40 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title></title>

    <style>
        .center {
            margin: auto;
            width: 20%;
            padding-top: 100px;
        }

        table, td, th {
            border: 1px solid black;
        }

        #table-collapse {
            border-collapse: collapse;
        }

        .btn-group button {
            background-color: #4CAF50;
            border: 1px solid green;
            color: white;
            padding: 10px 24px;
            cursor: pointer;
            width: 100%;
            display: block;
            font-size: 18px;
        }

        .btn-group button:not(:last-child) {
            border-bottom: none;
        }

        .btn-group button:hover {
            background-color: #3e8e41;
        }
    </style>
</head>
<body>
    <div class="center">
        <form action="mealTypes" method="post" class="btn-group" style="width: 100%;">
            <textarea style="width: 100%" id="mealTypeName" name="mealTypeName"></textarea>

            <br/>
            <br/>

            <table style="width: 100%;" class="btn-group" id="table-collapse">
                <tr style="border: 1px;">
                    <td style="width: 50%; text-align: center;">
                        <button formaction="/">Home</button>
                    </td>
                    <td style="width: 50%; text-align: center">
                        <c:if test="${action=='update'}">
                            <button id="mealTypeAction" name="mealTypeAction" value="update:${mealTypeId}" style="width: 100%">update</button>
                        </c:if>
                        <c:if test="${action=='add'}">
                            <button id="mealTypeAction" name="mealTypeAction" value="add" style="width: 100%">Add</button>
                        </c:if>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</body>
</html>
