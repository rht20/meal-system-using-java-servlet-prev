<%--
  Created by IntelliJ IDEA.
  User: rakibul.hasan
  Date: 3/1/20
  Time: 2:46 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title></title>

    <style>
        .center {
            margin: auto;
            width: 20%;
            padding-top: 100px;
        }

        table, td, th {
            border: 1px solid black;
        }

        #table-collapse {
            border-collapse: collapse;
        }

        .btn-group button {
            background-color: #4CAF50;
            border: 1px solid green;
            color: white;
            padding: 10px 24px;
            cursor: pointer;
            width: 100%;
            display: block;
            font-size: 18px;
        }

        .btn-group button:not(:last-child) {
            border-bottom: none;
        }

        .btn-group button:hover {
            background-color: #3e8e41;
        }
    </style>
</head>
<body>
    <div class="center">
        <h2 style="width: 100%; text-align: center">${day}, ${mealTypeName}</h2>
        <br/>

        <form action="menu" method="post" class="btn-group">
            <textarea id="mealTypeId" name="mealTypeId" hidden="hidden">${mealTypeId}</textarea>
            <textarea id="day" name="day" hidden="hidden">${day}</textarea>

            <c:forEach items="${items}" var="map">
                <c:if test="${map['status'] == true}">
                    <input type="checkbox" id="itemId" name="itemId" value="${map['itemId']}" checked>
                </c:if>
                <c:if test="${map['status'] == false}">
                    <input type="checkbox" id="itemId" name="itemId" value="${map['itemId']}">
                </c:if>

                <label style="font-size: 20px;">${map['itemName']}</label>
                <br/>
            </c:forEach>

            <br/>
            <br/>

            <table style="width: 100%;" id="table-collapse">
                <tr>
                    <td style="width: 50%;">
                        <button formaction="/">Home</button>
                    </td>
                    <td style="width: 50%;">
                        <button id="menuAction" name="menuAction" value="update">Update</button>
                    </td>
                </tr>
            </table>
        </form>




        <%--<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike">--%>
        <%--<label for="vehicle1"> I have a bike</label><br>--%>
        <%--<input type="checkbox" id="vehicle2" name="vehicle2" value="Car">--%>
        <%--<label for="vehicle2"> I have a car</label><br>--%>
        <%--<input type="checkbox" id="vehicle3" name="vehicle3" value="Boat">--%>
        <%--<label for="vehicle3"> I have a boat</label><br><br>--%>
        <%--<input type="submit" value="Submit">--%>
        <%--xxxxxx--%>
        <%--<h1 style="width: 100%; text-align: center">Available Items</h1>--%>
        <%--<table style="width: 100%" class="btn-group" id="table-collapse">--%>
            <%--<c:forEach items="${items}" var="item">--%>
                <%--<tr style="border: 1px">--%>
                    <%--<form action="items" method="post">--%>
                        <%--<td style="width: 34%; text-align: center;">--%>
                            <%--<h2>${item.name}</h2>--%>
                        <%--</td>--%>
                        <%--<td style="width: 33%; text-align: center">--%>
                            <%--<button id="itemAction" name="itemAction" value="updateCall:${item.id}">Update</button>--%>
                        <%--</td>--%>
                        <%--<td style="width: 33%; text-align: center">--%>
                            <%--<button id="itemAction" name="itemAction" value="remove:${item.id}">Remove</button>--%>
                        <%--</td>--%>
                    <%--</form>--%>
                <%--</tr>--%>
            <%--</c:forEach>--%>
        <%--</table>--%>

        <%--<br/>--%>
        <%--<br/>--%>
        <%--<br/>--%>

        <%--<table style="width: 100%;" class="btn-group" id="table-collapse">--%>
            <%--<tr style="border: 1px;">--%>
                <%--<form action="items" method="post">--%>
                    <%--<td style="width: 50%; text-align: center;">--%>
                        <%--<button formaction="/" id="itemAction" name="itemAction" value="Home">Home</button>--%>
                    <%--</td>--%>
                    <%--<td style="width: 50%; text-align: center">--%>
                        <%--<button id="itemAction" name="itemAction" value="addNewItem">Add New Item</button>--%>
                    <%--</td>--%>
                <%--</form>--%>
            <%--</tr>--%>
        <%--</table>--%>
    </div>
</body>
</html>
