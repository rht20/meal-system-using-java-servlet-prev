<%--
  Created by IntelliJ IDEA.
  User: rht_20
  Date: 2/26/20
  Time: 9:43 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Menu | Meal System</title>

    <style>
        .center {
            margin: auto;
            width: 40%;
            padding-top: 100px;
        }

        table, td, th {
            border: 1px solid black;
        }

        select {
            width: 100%;
            text-align: center;
            font-size: 18px;
            padding: 9px;
        }

        #table-collapse {
            border-collapse: collapse;
        }

        .btn-group button {
            background-color: #4CAF50;
            border: 1px solid green;
            color: white;
            padding: 10px 24px;
            cursor: pointer;
            width: 100%;
            display: block;
            font-size: 18px;
        }

        .btn-group button:not(:last-child) {
            border-bottom: none;
        }

        .btn-group button:hover {
            background-color: #3e8e41;
        }

        .xxx {
            background-color: #4CAF50;
            border: 1px solid green;
            color: white;
            padding: 10px 24px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 18px;
            margin: 4px 2px;
            cursor: pointer;
        }
    </style>
</head>
<body>
    <div class="center">
        <table style="width: 100%" class="btn-group" id="table-collapse">
            <tr style="width: 100%;">
                <td style="width: 20%; text-align: center">
                    <h3>Day</h3>
                </td>
                <td style="width: 20%; text-align: center">
                    <h3>Meal Type</h3>
                </td>
                <td style="width: 60%; text-align: center">
                    <h3>Items</h3>
                </td>
                <td style="width: 20%; text-align: center">
                    <h3>Remove</h3>
                </td>
            </tr>

            <c:forEach items="${menus}" var="menu">
                <tr>
                    <form action="menu" method="post" class="btn-group" style="width: 100%;">
                        <td style="text-align: center">${menu['day']}</td>
                        <td style="text-align: center">${menu['mealType']}</td>
                        <td style="text-align: center">${menu['items']}</td>
                        <td style="text-align: center">
                            <button id="menuAction" name="menuAction" value="${menu['menuId']}" style="width: 100%">Remove</button>
                        </td>
                    </form>
                </tr>
            </c:forEach>
        </table>

        <br/>
        <br/>

        <table style="width: 100%" class="btn-group" id="table-collapse">
            <tr>
                <form action="menu" method="post">
                    <td style="width: 40%;">
                        <select id="day" name="day" required="required">
                            <option value="">Select Day</option>

                            <c:forEach items="${days}" var="item">
                                <option value="${item}">${item}</option>
                            </c:forEach>
                        </select>
                    </td>

                    <td style="width: 40%;">
                        <select id="mealType" name="mealType" required="required">
                            <option value="">Select Meal Type</option>

                            <c:forEach items="${mealTypes}" var="item">
                                <option value="${item.id}:${item.name}">${item.name}</option>
                            </c:forEach>
                        </select>
                    </td>

                    <td style="width: 20%;">
                        <button id="menuAction" name="menuAction" value="updateCall">Submit</button>
                    </td>

                    <br/>
                    <br/>
                </form>
            </tr>
        </table>

        <br/>
        <br/>

        <a href="/" class="xxx">Home</a>
    </div>
</body>
</html>
