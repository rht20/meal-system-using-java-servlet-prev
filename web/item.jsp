<%--
  Created by IntelliJ IDEA.
  User: rht_20
  Date: 2/26/20
  Time: 9:35 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Items | Meal System</title>

    <style>
        .center {
            margin: auto;
            width: 30%;
            padding-top: 100px;
        }

        table, td, th {
            border: 1px solid black;
        }

        #table-collapse {
            border-collapse: collapse;
        }

        .btn-group button {
            background-color: #4CAF50;
            border: 1px solid green;
            color: white;
            padding: 10px 24px;
            cursor: pointer;
            width: 100%;
            display: block;
            font-size: 18px;
        }

        .btn-group button:not(:last-child) {
            border-bottom: none;
        }

        .btn-group button:hover {
            background-color: #3e8e41;
        }
    </style>
</head>
<body>
    <div class="center">
        <h1 style="width: 100%; text-align: center">Available Items</h1>
        <table style="width: 100%" class="btn-group" id="table-collapse">
            <c:forEach items="${items}" var="item">
                <tr style="border: 1px">
                    <form action="items" method="post">
                        <td style="width: 34%; text-align: center;">
                            <h2>${item.name}</h2>
                        </td>
                        <td style="width: 33%; text-align: center">
                            <button id="itemAction" name="itemAction" value="updateCall:${item.id}">Update</button>
                        </td>
                        <td style="width: 33%; text-align: center">
                            <button id="itemAction" name="itemAction" value="remove:${item.id}">Remove</button>
                        </td>
                    </form>
                </tr>
            </c:forEach>
        </table>

        <br/>
        <br/>
        <br/>

        <table style="width: 100%;" class="btn-group" id="table-collapse">
            <tr style="border: 1px;">
                <form action="items" method="post">
                    <td style="width: 50%; text-align: center;">
                        <button formaction="/">Home</button>
                    </td>
                    <td style="width: 50%; text-align: center">
                        <button id="itemAction" name="itemAction" value="addNewItem">Add New Item</button>
                    </td>
                </form>
            </tr>
        </table>
    </div>
</body>
</html>
