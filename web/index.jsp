<%--
  Created by IntelliJ IDEA.
  User: rakibul.hasan
  Date: 2/26/20
  Time: 2:42 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Meal System</title>

    <style>
        .center {
            margin: auto;
            width: 18%;
            padding-top: 100px;
        }

        .btn-group button {
            background-color: #4CAF50;
            border: 1px solid green;
            color: white;
            padding: 10px 24px;
            cursor: pointer;
            width: 100%;
            display: block;
            font-size: 18px;
        }

        .btn-group button:not(:last-child) {
            border-bottom: none;
        }

        .btn-group button:hover {
            background-color: #3e8e41;
        }
    </style>
</head>
<body>
    <div class="center">
        <h1 style="width: 100%; text-align: center">Meal System</h1>
        <form method="post" class="btn-group">
            <button formaction="items" id="itemAction" name="itemAction" value="show">Items</button>
            <br/>
            <button formaction="mealTypes" id="mealTypeAction" name="mealTypeAction" value="show">Meal Types</button>
            <br/>
            <button formaction="menu" id="menuAction" name="menuAction" value="show">Menu</button>
            <br/>
        </form>
    </div>
</body>
</html>
